package com.client.bank.hw_spring.service;

import com.client.bank.hw_spring.dao.AccountDAO;
import com.client.bank.hw_spring.domain.Account;
import com.client.bank.hw_spring.domain.Currency;
import com.client.bank.hw_spring.domain.Customer;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.concurrent.CancellationException;

@Service
@AllArgsConstructor
public class AccountService {
    private AccountDAO dao;

    public List<Account> getAllAccounts() {
        return dao.findAll();
    }

    public void topUpTheAccount(String accNumber, Double ammount) throws NoSuchElementException {
        Account acc = getAccountByNumber(accNumber);
        acc.setBalance(acc.getBalance() + ammount);
    }

    public void withdrawFromAccount(String custId, String accNumber, Double ammount) throws NoSuchElementException,
            IllegalAccessError {
        Account acc = getAccountByNumber(accNumber);
        if (acc.getCustomer().getId() != Long.valueOf(custId)) throw new IllegalAccessError("Access Error");
        acc.setBalance(acc.getBalance() - ammount);
    }

    public void transferMoneyBetweenAccounts(String custId, String senderNumber, String recipientNumber, Double ammount)
            throws CancellationException, NoSuchElementException, IllegalAccessError {
        Account sender = getAccountByNumber(senderNumber);
        if (sender.getId() != Long.valueOf(custId)) throw new IllegalAccessError("Access Error");
        Account recipient = getAccountByNumber(recipientNumber);

        if (sender.getBalance() < ammount) throw new CancellationException("There are not enough money on account!");
        else {
            sender.setBalance(sender.getBalance() - ammount);
            recipient.setBalance(recipient.getBalance() + ammount);
        }

    }

    public Account getAccountByNumber(String num) throws NoSuchElementException {
        Optional<Account> acc = dao.findByNumber(num);
        if (acc.isEmpty()) throw new NoSuchElementException(String.format("There are not account number №%s!", num));
        else return acc.get();
    }

    public Account getAccountById(String custId, String id) throws NoSuchElementException, NumberFormatException {
        Long formattedId = Long.valueOf(id);
        Long formattedCustId = Long.valueOf(custId);
        Optional<Account> account = dao.getOne(formattedCustId, formattedId);
        if (account.isEmpty())
            throw new NoSuchElementException(String.format("There are not account with id %d!", formattedId));
        else return account.get();
    }

    public Account getAccountById(Long custId, Long id) throws NoSuchElementException {
        Optional<Account> account = dao.getOne(custId, id);
        if (account.isEmpty()) throw new NoSuchElementException(String.format("There are not account with id %d!", id));
        else return account.get();
    }

    public List<Account> getAccountsByCustomer(String customerId) throws NoSuchElementException, NumberFormatException {
        Long formattedId = Long.valueOf(customerId);
        List<Account> accounts = dao.findAll(formattedId);
        if (accounts.isEmpty()) throw new NoSuchElementException(
                String.format("There are not accounts for customer with id %d!", formattedId)
        );
        return accounts;
    }

    public Account createNewAccount(Currency currency, Customer customer) {
        return dao.save(new Account(currency, customer));
    }

    public boolean deleteAccount(Long id) {
        return dao.deleteById(id);
    }
}
