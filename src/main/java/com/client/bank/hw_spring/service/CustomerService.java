package com.client.bank.hw_spring.service;

import com.client.bank.hw_spring.dao.CustomerDAO;
import com.client.bank.hw_spring.domain.Account;
import com.client.bank.hw_spring.domain.CreateAccountDTO;
import com.client.bank.hw_spring.domain.Currency;
import com.client.bank.hw_spring.domain.Customer;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.Level;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@AllArgsConstructor
@Service
@Log4j2
public class CustomerService {
    private CustomerDAO dao;
    private AccountService accService;


    public Customer getCustomerInfo(String id) throws NoSuchElementException, NumberFormatException {
        Long formattedId = Long.valueOf(id);
        Optional<Customer> customer = dao.getOne(formattedId);
        if (customer.isEmpty())
            throw new NoSuchElementException(String.format("There are not customer with id %d!", formattedId));
        else return customer.get();
    }

    public Customer getCustomerInfo(Long id) throws NoSuchElementException {
        Optional<Customer> customer = dao.getOne(id);
        if (customer.isEmpty())
            throw new NoSuchElementException(String.format("There are not customer with id %d!", id));
        else return customer.get();
    }

    public List<Customer> getAllCustomers() {
        List<Customer> allCustomers = dao.findAll();
        if (allCustomers.isEmpty()) generateTestData();
        return dao.findAll();
    }

    public Customer createNewCustomer(String name, String email, Integer age) {
        Customer newCustomer = new Customer(name, email, age);
        return dao.save(newCustomer);
    }

    public Customer updateCustomerName(Long id, String name, String email) throws NoSuchElementException {
        Customer customer = getCustomerInfo(id);
        if (!name.isEmpty()) customer.setName(name);
        else if (!email.isEmpty()) customer.setEmail(email);
        return dao.save(customer);
    }

    public boolean deleteCustomer(Long id) {
        return dao.deleteById(id);
    }

    public boolean deleteCustomer(String id) {
        return dao.deleteById(Long.valueOf(id));
    }

    public Customer createAccountForCustomer(String customerId, String currency) throws IllegalArgumentException {
        Currency curr = Currency.valueOf(currency);
        Customer customer = getCustomerInfo(customerId);

        Account acc = accService.createNewAccount(curr, customer);
        customer.addAccount(acc);

        return dao.save(customer);
    }

    public Customer createAccountForCustomer(Long customerId, String currency) throws IllegalArgumentException {
        Currency curr = Currency.valueOf(currency);
        Customer customer = getCustomerInfo(customerId);

        Account acc = accService.createNewAccount(curr, customer);
        customer.addAccount(acc);

        return dao.save(customer);
    }

    public boolean deleteAccountByNumber(String customerId, String number) {
        try {
            Customer cust = getCustomerInfo(customerId);
            Account acc = accService.getAccountByNumber(number);
            cust.deleteAccount(acc);
            accService.deleteAccount(acc.getId());
            dao.save(cust);
            return true;
        } catch (NoSuchElementException ex) {
            return false;
        }
    }

    public Customer updateCustomer(String customerId, Customer updatedCustomer) {
        Customer existingCustomer = getCustomerInfo(customerId);
        if (existingCustomer != null) {
            existingCustomer.setName(updatedCustomer.getName());
            existingCustomer.setEmail(updatedCustomer.getEmail());
            existingCustomer.setAge(updatedCustomer.getAge());

            return dao.save(existingCustomer);
        } else {
            return null;
        }
    }

    public void generateTestData() {
        Customer c1 = createNewCustomer("Ivan Petrow", "petrow@gmail.com", 25);
        createAccountForCustomer(c1.getId(), "USD");
        createAccountForCustomer(c1.getId(), "EUR");

        Customer c2 = createNewCustomer("John Johnson", "johnson@gmail.com", 45);
        createAccountForCustomer(c2.getId(), "USD");
        createAccountForCustomer(c2.getId(), "EUR");

        Customer c3 = createNewCustomer("Sem Frid", "frid@gmail.com", 45);
        createAccountForCustomer(c3.getId(), "GBP");
        createAccountForCustomer(c3.getId(), "EUR");
    }

}

