package com.client.bank.hw_spring.controller;

import com.client.bank.hw_spring.domain.Account;
import com.client.bank.hw_spring.domain.AccountTransferDTO;
import com.client.bank.hw_spring.domain.CreateAccountDTO;
import com.client.bank.hw_spring.domain.Customer;
import com.client.bank.hw_spring.service.AccountService;
import com.client.bank.hw_spring.service.CustomerService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/api/customers/{customerId}/accounts")
public class AccountController {
    private AccountService service;
    private CustomerService customerService;

    @GetMapping("")
    public List<Account> getAccounts(@PathVariable String customerId) {
        return service.getAccountsByCustomer(customerId);
    }

    @GetMapping("{id}")
    public Account getAccountById(@PathVariable String id, @PathVariable String customerId) {
        return service.getAccountById(customerId, id);
    }

    @PostMapping("")
    public Customer createNewAccount(@PathVariable String customerId, @RequestBody CreateAccountDTO createAccDTO) {
        String currency = createAccDTO.getCurrency();
        return customerService.createAccountForCustomer(customerId, currency);
    }

    @DeleteMapping("")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public boolean deleteAccount(@PathVariable String customerId, @RequestParam String number) {
        return customerService.deleteAccountByNumber(customerId, number);
    }

    @PutMapping("topUP")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void topUpAccount(@PathVariable String customerId, @RequestParam String number, @RequestParam String amount) {
        service.topUpTheAccount(number, Double.valueOf(amount));
    }

    @PutMapping("transfer")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void transfer(@PathVariable String customerId, @RequestBody AccountTransferDTO accTransferDTO) {
        service.transferMoneyBetweenAccounts(customerId, accTransferDTO.getSender(), accTransferDTO.getRecepient(), accTransferDTO.getAmount());
    }

    @PutMapping("withdraw")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void withdraw(@PathVariable String customerId, @RequestParam String number, @RequestParam String amount) {
        service.withdrawFromAccount(customerId, number, Double.valueOf(amount));
    }

}
