package com.client.bank.hw_spring.controller;

import com.client.bank.hw_spring.domain.Customer;
import com.client.bank.hw_spring.service.CustomerService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/api/customers")
public class CustomerController {
    private CustomerService service;

    @GetMapping("")
    public List<Customer> getAllCustomer() {
        return service.getAllCustomers();
    }

    @GetMapping("{id}")
    public Customer getCustomerById(@PathVariable String id) {
        return service.getCustomerInfo(id);
    }

    @PutMapping("{id}")
    public ResponseEntity<Customer> updateCustomer(
            @PathVariable String customerId,
            @RequestBody Customer updatedCustomer
    ) {
        Customer updated = service.updateCustomer(customerId, updatedCustomer);
        if (updatedCustomer != null) {
            return ResponseEntity.ok(updated);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("")
    public ResponseEntity<Customer> createCustomer(
            @RequestBody Customer updatedCustomer
    ) {
        Customer newCustomer = service.createNewCustomer(updatedCustomer.getName(), updatedCustomer.getEmail(), updatedCustomer.getAge());
        return ResponseEntity.ok(newCustomer);
    }

    @DeleteMapping("{id}")
    public ResponseEntity deleteCustomer(@PathVariable String id) {
        boolean result = service.deleteCustomer(id);
        return result ? ResponseEntity.ok(true) : ResponseEntity.notFound().build();
    }

}
