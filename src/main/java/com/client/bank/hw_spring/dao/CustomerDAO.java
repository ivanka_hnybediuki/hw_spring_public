package com.client.bank.hw_spring.dao;

import com.client.bank.hw_spring.domain.Customer;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Log4j2
@Repository
public class CustomerDAO implements ListStorageDAO<Customer> {
    private static Long idCounter = Long.valueOf(1);
    private List<Customer> data = new ArrayList<>();

    public CustomerDAO() {
        data = new ArrayList<>();
    }

    @Override
    public List<Customer> getDataList() {
        return data;
    }

    @Override
    public Customer save(Customer obj) {
        int i = this.data.indexOf(obj);
        if (i < 0) {
            obj.setId(CustomerDAO.idCounter++);
            this.data.add(obj);
        } else this.data.set(i, obj);
        return obj;
    }
}
