package com.client.bank.hw_spring.dao;

import com.client.bank.hw_spring.domain.Account;
import com.client.bank.hw_spring.domain.Customer;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class AccountDAO implements ListStorageDAO<Account> {
    private static Long idCounter = Long.valueOf(1);
    private List<Account> data = new ArrayList<>();

    @Override
    public List<Account> getDataList() {
        return data;
    }

    @Override
    public Account save(Account obj) {
        int i = this.data.indexOf(obj);
        if (i < 0) {
            obj.setId(AccountDAO.idCounter++);
            this.data.add(obj);
        } else this.data.set(i, obj);
        return obj;
    }

    public Optional<Account> findByNumber(String num) {
        return data.stream().filter((Account acc) -> acc.getNumber().equals(num)).findAny();
    }

    public List<Account> findAll(Long customerId) {
        return data.stream().filter(acc -> acc.getCustomer().getId() == customerId).toList();
    }

    public Optional<Account> getOne(long custid, long id) {
        return findAll(custid).stream()
                .filter(entity -> entity.getId() == id)
                .findAny();
    }
}
