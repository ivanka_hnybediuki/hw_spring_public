package com.client.bank.hw_spring.dao;

import com.client.bank.hw_spring.domain.Identifieble;

import java.util.List;
import java.util.Optional;


interface ListStorageDAO<T extends Identifieble> extends DAO<T> {
    List<T> getDataList();

    T save(T obj);

    default boolean delete(T obj) {
        return getDataList().remove(obj);
    }

    default void deleteAll(List<T> entities) {
        entities.forEach(entity -> delete(entity));
    }

    default void saveAll(List<T> entities) {
        entities.forEach(entity -> save(entity));
    }

    default List<T> findAll() {
        return this.getDataList();
    }

    default boolean deleteById(long id) {
        Optional<T> customerOptional = getOne(id);

        return customerOptional.isPresent()
                ? delete(customerOptional.get())
                : false;
    }

    default Optional<T> getOne(long id) {
        return getDataList().stream()
                .filter(entity -> entity.getId() == id)
                .findAny();
    }
}
