package com.client.bank.hw_spring.dao;

import java.util.List;
import java.util.Optional;

import com.client.bank.hw_spring.domain.Identifieble;

public interface DAO<T extends Identifieble> {
    T save(T obj);

    boolean delete(T obj);

    void deleteAll(List<T> entities);

    void saveAll(List<T> entities);

    List<T> findAll();

    boolean deleteById(long id);

    Optional<T> getOne(long id);
}
