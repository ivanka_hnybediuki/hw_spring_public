package com.client.bank.hw_spring.domain;

import lombok.*;

@NoArgsConstructor
@Data
public class AccountTransferDTO {
    private String sender;
    private String recepient;
    private double amount;
}
