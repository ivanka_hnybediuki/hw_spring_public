package com.client.bank.hw_spring.domain;

public enum Currency {
    USD,
    EUR,
    UAH,
    CHF,
    GBP
}
