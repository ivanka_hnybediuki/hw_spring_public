package com.client.bank.hw_spring.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Objects;
import java.util.UUID;

@Data
public class Account implements Identifieble {
    private Long id;
    private String number;
    private Currency currency;
    private Double balance;
    @JsonIgnore
    private Customer customer;

    public Account(Currency currency, Customer customer) {
        this.number = Account.getNewAccountNumber();
        this.currency = currency;
        this.balance = 0.0;
        this.customer = customer;
    }

    static String getNewAccountNumber() {
        return UUID.randomUUID().toString();
    }

    @Override
    public String toString() {
        return "Account{" +
                "id: " + id +
                ", number: '" + number + '\'' +
                ", currency: " + currency +
                ", balance: " + balance +
                ",\ncustomer: \n" + customer +
                '}';
    }
}
