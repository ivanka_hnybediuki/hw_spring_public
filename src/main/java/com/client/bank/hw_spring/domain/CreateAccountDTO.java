package com.client.bank.hw_spring.domain;

import lombok.*;

@Data
@NoArgsConstructor
public class CreateAccountDTO {
    private String currency;
}
