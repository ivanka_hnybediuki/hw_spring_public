package com.client.bank.hw_spring.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Data
public class Customer implements Identifieble {
    private Long id;
    private String name;
    private String email;
    private Integer age;
    private List<Account> accounts;

    @JsonCreator
    public Customer(@JsonProperty("name") String name, @JsonProperty("email") String email, @JsonProperty("age") int age) {
        this.name = name;
        this.age = age;
        this.email = email;
        this.accounts = new ArrayList<>();
    }

    public Customer(String name, String email, Integer age) {
        this.name = name;
        this.email = email;
        this.age = age;
        this.accounts = new ArrayList<>();
    }

    @Override
    public String toString() {
        StringBuilder res = new StringBuilder(String.format("NAME: %s%nEMAIL: %s%nACCOUNTS:%n", this.name, this.email));
        this.accounts.forEach(acc -> res.append(acc.toString()));
        res.append("\n");
        return res.toString();
    }

    public void addAccount(Account acc) {
        accounts.add(acc);
    }

    public boolean deleteAccount(Account acc) {
        return accounts.remove(acc);
    }
}
